<?php

/*
    Plugin Name: Premium GPL Helper
    Description: This plugin makes it easy to work with premium WP plugins that are explicitly GPL, without all the vendor warnings.
    Version: 1.3
    Author: Dr.Wayne Buckhanan
    Author URI: http://mercs.net/
*/

// Remove AffiliateWP license reminder
// bypass Affiliate_WP_Settings->check_license by caching a valid result
set_transient( 'affwp_license_check', 'valid', 0 );

// Remove WooThemes Updater notification message
// Courtesy of Steve Johnson via 
//   https://github.com/wpspring/Remove-Woothemes-Updater-Plugin-Notice
// GPLv2 or later

if(!class_exists('WC_RemoveWoothemesUpdaterPluginNotice')) :

class WC_RemoveWoothemesUpdaterPluginNotice {
    public function __construct() {
        add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
    }
    public function plugins_loaded() {
        if ( is_admin() ) {
            remove_action('admin_notices', 'woothemes_updater_notice', 10);
        }
    }
}
$GLOBALS['wc_removewoothemesupdaterpluginnotice'] = new WC_RemoveWoothemesUpdaterPluginNotice();
endif;
// end Johnson block

// Remove WPMUdev Updater notification message
// Courtesy of Paul Hirst via
//   https://wordpress.org/plugins/wpmu-no-nag/
// GPLv2 or later

if ( ! class_exists('WPMUDEV_Update_Notifications') ) :
class WPMUDEV_Update_Notifications {
      public function __construct()
      {
      }
}
endif;
    
if ( ! class_exists('WPMUDEV_Dashboard') ) :
class WPMUDEV_Dashboard {
      public function __construct()
      {
      }
}
endif;

if ( class_exists('WPMUDEV_Dashboard_Notice3') ) {
   remove_action( 'all_admin_notices', array( $WPMUDEV_Dashboard_Notice3, 'activate_notice' ), 10 );
   remove_action( 'all_admin_notices', array( $WPMUDEV_Dashboard_Notice3, 'install_notice' ), 10 );
}

/* need reworked to not disable other plugins too!
* function my_plugin_load_last()
* {
* 	$path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );
* 	if ( $plugins = get_option( 'active_plugins' ) ) {
* 		if ( $key = array_search( $path, $plugins ) ) {
* 			array_splice( $plugins, $key, 9999 );
* 			array_unshift( $plugins, $path );
* 			update_option( 'active_plugins', $plugins );
* 		}
* 	}
* }
* add_action( 'activated_plugin', 'my_plugin_load_last' );
*/
// end Hirst block

// Overwrite existing filename on upload
// Courtesy of Ian Dunn via
//   https://wordpress.org/plugins/overwrite-uploads/developers/
// GPLv2

/* REQUIRED_PHP_VERSION: '5.2.4' // Because of WP minimum requirements */
/* REQUIRED_WP_VERSION: '2.9' // Because of wp_handle_upload_prefilter */

/**
 * Overwrites uploaded files that already exist, instead of storing multiple copies.
 */
class OverwriteUploads {
	/**
	 * Constructor
	 */
	public function __construct() {
		add_filter( 'wp_handle_upload_prefilter', array( $this, 'remove_existing_attachment' ) );    // Not really the appropriate hook, but there isn't an action that fits
	}

	/**
	 * Remove a existing attachment when uploading a new one with the same name in the same folder
	 * 
	 * @param array $file
	 * @return array The unmodified file
	 */
	public function remove_existing_attachment( $file ) {
		$uploads_dir = wp_upload_dir();
		
		if ( file_exists( $uploads_dir['path'] . DIRECTORY_SEPARATOR . $file['name'] ) ) {
			$params = array(
				'numberposts'   => 1,
				'post_type'     => 'attachment',
				'meta_query'    => array(
					array(
						'key'   => '_wp_attached_file',
						'value' => trim( $uploads_dir['subdir'] . DIRECTORY_SEPARATOR . $file['name'], DIRECTORY_SEPARATOR )
					)
				)
			);

			$existing_file = get_posts( $params );
			
			if ( isset( $existing_file[0]->ID ) ) {
				wp_delete_attachment( $existing_file[0]->ID, true );
			}
		}
		
		return $file;
	}
} 

$GLOBALS['ovup'] = new OverwriteUploads();
// end Dunn block

// Prompt to overwrite existing plugins on install
// Courtesy of Chris Jean via
//   https://wordpress.org/plugins/easy-theme-and-plugin-upgrades/
// Unknown OSS license - in WP.org repo

if ( is_admin() )
{
	if ( ! class_exists( 'ETUModifyInstaller' ) ) {
		class ETUModifyInstaller {
			var $_errors = array();
			var $_type = '';
			
			function ETUModifyInstaller() {
				if ( preg_match( '/update\.php/', $_SERVER['REQUEST_URI'] ) && isset( $_REQUEST['action'] ) ) {
					if ( 'upload-theme' === $_REQUEST['action'] ) {
						$this->_type = 'theme';
					} else if ( 'upload-plugin' === $_REQUEST['action'] ) {
						$this->_type = 'plugin';
					}
					
					if ( ! empty( $this->_type ) ) {
						add_action( 'admin_init', array( $this, 'handle_upgrades' ), 100 );
					}
				}
				
				add_action( 'load-theme-install.php', array( $this, 'start_theme_output_buffering' ) );
				add_action( 'load-plugin-install.php', array( $this, 'start_plugin_output_buffering' ) );
			}
			
			function filter_output( $output ) {
				$text = "<div style='max-width:600px;'>\n";
				$text .= "<p><i>By default, the installer will not overwrite an existing {$this->_type}. Change the following option to \"Yes\" to allow this installer to perform upgrades as well.</i></p>";
				$text .= "<p>Upgrade existing {$this->_type}? <select name='caj_etu_upgrade_existing'><option value=''>No</option><option value='yes'>Yes</option></select></p>\n";
				$text .= "<p>If a {$this->_type} is upgraded, the following process will be used:</p>\n";
				$text .= "<ol>\n";
				$text .= "<li>A backup zip of the existing {$this->_type} will be created and added to the <a href='" . admin_url( 'upload.php' ) . "'>Media Library</a>.</li>\n";
				$text .= "</ol><br />\n";
				$text .= "</div>\n";
				
				$output = preg_replace( '/(<input [^>]*name="(?:theme|plugin)zip".+?\n)/', "\$1$text", $output );
				
				return $output;
			}
			
			function start_theme_output_buffering() {
				$this->_type = 'theme';
				ob_start( array( $this, 'filter_output' ) );
			}
			
			function start_plugin_output_buffering() {
				$this->_type = 'plugin';
				ob_start( array( $this, 'filter_output' ) );
			}
			
			function _get_themes() {
				global $wp_themes;
				
				if ( isset( $wp_themes ) ) {
					return $wp_themes;
				}
				
				$themes = wp_get_themes();
				$wp_themes = array();
				
				foreach ( $themes as $theme ) {
					$name = $theme->get( 'Name' );
					if ( isset( $wp_themes[$name] ) )
						$wp_themes[$name . '/' . $theme->get_stylesheet()] = $theme;
					else
						$wp_themes[$name] = $theme;
				}
				
				return $wp_themes;
			}
			
			function _get_theme_data( $directory ) {
				$data = array();
				
				$themes = $this->_get_themes();
				$active_theme = wp_get_theme();
				$current_theme = array();
				
				foreach ( (array) $themes as $theme_name => $theme_data ) {
					if ( $directory === $theme_data['Stylesheet'] )
						$current_theme = $theme_data;
				}
				
				if ( empty( $current_theme ) )
					return $data;
				
				$data['version'] = $current_theme['Version'];
				$data['name'] = $current_theme['Name'];
				$data['directory'] = $current_theme['Stylesheet Dir'];
				
				$data['is_active'] = false;
				if ( ( $active_theme->template_dir === $current_theme['Template Dir'] ) || ( $active_theme->template_dir === $current_theme['Template Dir'] ) )
					$data['is_active'] = true;
				
				global $wp_version;
				if ( version_compare( '2.8.6', $wp_version, '>' ) )
					$data['directory'] = WP_CONTENT_DIR . $current_theme['Stylesheet Dir'];
				
				return $data;
			}
			
			function _get_plugin_data( $directory ) {
				$data = array();
				
				$plugins = get_plugins();
				$active_plugins = get_option('active_plugins');
				$current_plugin = array();
				
				foreach ( (array) $plugins as $plugin_path_file => $plugin_data ) {
					$path_parts = explode( '/', $plugin_path_file );
					if ( $directory === reset( $path_parts ) ) {
						$current_plugin = array( 'path' => $plugin_path_file, 'data' => $plugin_data );
					}
				}
				
				if ( empty( $current_plugin ) ) {
					return $data;
				}
				
				$data['version'] = $current_plugin['data']['Version'];
				$data['name'] = $current_plugin['data']['Name'];
				$data['directory'] = WP_PLUGIN_DIR . '/' . $directory;
				$data['is_active'] = ( is_plugin_active( $current_plugin['path'] ) ) ? true : false;
				
				return $data;
			}
			
			function handle_upgrades() {
				if ( empty( $_POST['caj_etu_upgrade_existing'] ) ) {
					$this->_errors[] = "The Easy Theme and Plugin Upgrades plugin was unable to handle requests for this upgrade. Unfortunately, this setup may be incompatible with the plugin.";
					add_action( 'admin_notices', array( $this, 'show_upgrade_option_error_message' ) );
					
					return;
				}
				
				if ( 'yes' !== $_POST['caj_etu_upgrade_existing'] ) {
					if ( 'plugin' == $this->_type ) {
						$link = admin_url( "plugin-install.php?tab=upload" );
					} else if ( version_compare( $GLOBALS['wp_version'], '3.8.9', '>' ) ) {
						$link = admin_url( "theme-install.php" );
					} else {
						$link = admin_url( "theme-install.php?tab=upload" );
					}
					
					$this->_errors[] = "You must select \"Yes\" from the \"Upgrade existing {$this->_type}?\" dropdown option in order to upgrade an existing {$this->_type}. <a href=\"$link\">Try again</a>.";
					add_action( 'admin_notices', array( $this, 'show_upgrade_option_error_message' ) );
					
					return;
				}
				
				remove_action( 'admin_print_styles', 'builder_add_global_admin_styles' );
				
				
				include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
				require_once( ABSPATH . 'wp-admin/includes/class-pclzip.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				
				check_admin_referer( "{$this->_type}-upload" );
				@set_time_limit( 300 );
				
				$archive = new PclZip( $_FILES["{$this->_type}zip"]['tmp_name'] );
				
				$directory = '';
				$contents = $archive->listContent();
				
				foreach ( (array) $contents as $content ) {
					if ( preg_match( '|^([^/]+)/$|', $content['filename'], $matches ) ) {
						$directory = $matches[1];
						break;
					}
				}
				
				if ( 'theme' === $this->_type )
					$data = $this->_get_theme_data( $directory );
				else if ( 'plugin' === $this->_type )
					$data = $this->_get_plugin_data( $directory );
				
				if ( empty( $data ) )
					return;
				
				
				$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
				$rand_string = '';
				$length = rand( 10, 20 );
				for ( $count = 0; $count < $length; $count++ )
					$rand_string .= $characters[rand( 0, strlen( $characters ) - 1 )];
				
				$zip_file = "$directory-{$data['version']}-$rand_string.zip";
				
				$wp_upload_dir = wp_upload_dir();
				$zip_path = $wp_upload_dir['path'] . '/' . $zip_file;
				$zip_url = $wp_upload_dir['url'] . '/' . $zip_file;
				
				$archive = new PclZip( $zip_path );
				
				$zip_result = $archive->create( $data['directory'], PCLZIP_OPT_REMOVE_PATH, dirname( $data['directory'] ) );
				
				if ( 0 == $zip_result ) {
					$this->_errors[] = "Unable to make a backup of the existing {$this->_type}. Will not proceed with the upgrade.";
					add_action( 'admin_notices', array( $this, 'show_upgrade_option_error_message' ) );
					return;
				}
				
				
				$attachment = array(
					'post_mime_type'	=> 'application/zip',
					'guid'				=> $zip_url,
					'post_title'		=> ucfirst( $this->_type ) . " Backup - {$data['name']} - {$data['version']}",
					'post_content'		=> '',
				);
				
				$id = wp_insert_attachment( $attachment, $zip_path );
				if ( !is_wp_error( $id ) )
					wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $zip_path ) );
				
				
				if ( $data['is_active'] )
					set_transient( 'etu-in-maintenance-mode', '1', 300 );
				
				global $wp_filesystem;
				
				if ( ! WP_Filesystem() ) {
					$this->_errors[] = 'Unable to initialize WP_Filesystem. Will not proceed with the upgrade.';
					add_action( 'admin_notices', array( $this, 'show_upgrade_option_error_message' ) );
					
					return;
				}
				
				if ( ! $wp_filesystem->delete( $data['directory'], true ) ) {
					$this->_errors[] = "Unable to remove the existing {$this->_type} directory. Will not proceed with the upgrade.";
					add_action( 'admin_notices', array( $this, 'show_upgrade_option_error_message' ) );
					
					return;
				}
				
				$this->_zip_url = $zip_url;
				
				add_action( 'all_admin_notices', array( $this, 'show_message' ) );
			}
			
			function show_message() {
				echo "<div id=\"message\" class=\"updated fade\"><p><strong>A backup zip file of the old {$this->_type} version can be downloaded <a href='$this->_zip_url'>here</a>.</strong></p></div>\n";
				
				delete_transient( 'etu-in-maintenance-mode' );
			}
			
			function show_upgrade_option_error_message() {
				if ( ! isset( $this->_errors ) )
					return;
				
				if ( ! is_array( $this->_errors ) )
					$this->_errors = array( $this->_errors );
				
				foreach ( (array) $this->_errors as $error )
					echo "<div id=\"message\" class=\"error\"><p><strong>$error</strong></p></div>\n";
			}
		}
		
		new ETUModifyInstaller();
	}
} // endif ( is_admin() )
// end Jean block

?>
