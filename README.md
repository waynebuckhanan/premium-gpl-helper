# Premium GPL Helper

This WordPress plugin makes it easy to work with premium WP plugins that are explicitly GPL, without all the vendor warnings.

Currently bypasses the warning stripes for plugins from:

- AffiliateWP
- WooThemes
- WPMUdev

And allows for uploading a different version of a plugin over the top of a previously installed plugin (which WP normally prevents).

Thanks to snippets from:

- [Steve Johnson](https://github.com/wpspring/Remove-Woothemes-Updater-Plugin-Notice)
- [Paul Hirst](https://wordpress.org/plugins/wpmu-no-nag/)
- [Ian Dunn](https://wordpress.org/plugins/overwrite-uploads/developers/)
- [Chris Jean via](https://wordpress.org/plugins/easy-theme-and-plugin-upgrades/)

-- Dr.Wayne Buckhanan
